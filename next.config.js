const withAntdLess = require('next-plugin-antd-less');
const withBundleAnalyzer = require('@next/bundle-analyzer')({ enabled: process.env.ANALYZE === 'true' });

module.exports = withBundleAnalyzer(withAntdLess({
    // optional
    modifyVars: {},
    // optional https://github.com/webpack-contrib/css-loader#object
    cssLoaderOptions: {},

    webpack(config) {
        return config;
    },

    future: {
        // if you use webpack5
        webpack5: true,
    },
}));